#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/sysinfo.h>

#define MAXLINE 256
#define VERSION "0.0.1"

bool verbose;
struct sysinfo sys_info;

struct bat {
    char *name;
    char *value;
    struct bat *next;
};

struct bat *head = NULL;
struct bat *temp = NULL;
struct bat *tail = NULL;

void addBatNode(char *name, char *value) {

    temp = (struct bat *) malloc(sizeof(struct bat));

    // Add link to head if NULL
    if (head == NULL) {
        head = temp;
        head->name = name;
        head->value = value;
        tail = head;
    } else {
        temp->name = name;
        temp->value = value;
        tail->next = temp;
        tail = temp;
    }
}

void printBatList() {

    if (verbose) {
        printf("%-35s %s\n", "Name", "Value");
        printf("%-35s %s\n", "----", "----");
    }

    while(head) {
        printf("%-35s %s\n", head->name, head->value);
        head = head->next;
    }
    puts("");
}

void help(char *argv[]) {
    printf("\
Usage:\n\
  %s [OPTION] Machine Learning Battery Tool\n\
\n\
Options:\n\
  -h       Show help options.\n\
  -V       Verbose mode. Default: off\n\
  -v       Display version number.\n\
\n", argv[0]);

    exit(EXIT_SUCCESS);
}

void version(char *argv[]) {
    printf("%s v%s\n", argv[0], VERSION);
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {

    int c;

    while (( c = getopt (argc, argv, "hvV")) != -1) {
        switch (c) {
            case 'h' :
                help(argv);
                break;
            case 'v' :
                version(argv);
                break;
            case 'V' :
                verbose = true;
                break;
            default:
                printf("Try '%s -h' for more information.\n", argv[0]);
                exit(EXIT_SUCCESS);
        }
    }

    // File setup
    FILE *fp;
    char buffer[MAXLINE];
    const char *filepath = "/sys/class/power_supply/BAT0/uevent";

    // Token setup
    char *name;
    char *value;
    char delim[] = "=\n";

    // System Info setup

    // Open Battery uevent file
    fp = fopen(filepath, "r");
    if (fp == NULL) {
        printf("mlbat: cannot access %s: No such file\n", filepath);
        exit(EXIT_FAILURE);
    }

    if (verbose) {
        printf("\nSuccesfully opened: %s\n\n", filepath);
    }

    // Tokenize file
    while (fgets(buffer, sizeof(buffer), fp)) {

        name = strtok(buffer, delim);
        value = strtok(NULL, delim);

        // Add to bat list
        addBatNode(strdup(name), strdup(value));

    }

    printBatList();

    if (verbose) {
        sysinfo(&sys_info);
        printf("uptime(secs): %-10lu load avg(1m): %-10lu current # of procs: %hu\n\n",
            sys_info.uptime, sys_info.loads[0], sys_info.procs);
    }

    // Clean file stream and exit
    fclose(fp);
    exit(EXIT_SUCCESS);
}

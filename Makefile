#Makefile for glob

CC = gcc
CFLAGS = -Wall -ggdb

TARGET1 = mlbat
TARGET1_SRC = mlbat.c
 

default: $(TARGET1)

all: $(TARGET1)

$(TARGET1): $(TARGET1_SRC)
	$(CC) $(CFLAGS) $(TARGET1_SRC) -o $(TARGET1)

test:
	$(PWD)/$(TARGET1)

clean:
	rm -f $(TARGET1)

